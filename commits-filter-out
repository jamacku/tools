#!/bin/bash

if [[ $1 == "-h" || $1 == "--help" ]]; then
	echo "Usage: commits-filter-out <excludes_file> [commits_file]"
	echo "Removes the commits listed in excludes_file from commits_file"
	echo "If no 'commits_file' argument is provided, stdin will be read."\
	     "Passing '-' as any of the filenames will make stdin to be read."
	echo "Both files must be lists of commits hashes"
	exit
elif (( $# >= 2 )); then
	excludes_file="$1"
	commits_file="$2"
elif (( $# >= 1 )); then
	excludes_file="$1"
	commits_file="/dev/stdin"
else
	echo "Error: at least 1 argument required" >&2
	exit 1
fi

# interpret '-' arguments as /dev/stdin
[[ $excludes_file = "-" ]] && excludes_file="/dev/stdin"
[[ $commits_file = "-" ]] && commits_file="/dev/stdin"

if [[ $excludes_file = "/dev/stdin" && $commits_file = "/dev/stdin" ]]; then
	echo "Error: can't read both files from stdin" >&2
	exit 1
fi

# get commits list, since we need to read it twice
commits="$(<"$commits_file")"

# find min number of characters in hashes
min_len=40
for commit in $(echo "$commits" | awk '{print $1}'); do
	len=${#commit}
	(( $len < $min_len )) && min_len=$len
done

# get excluded commits with hashes of length equal or less to min_len
excludes="$(awk -v len=$min_len '{print "^" substr($1,0,len)}' < "$excludes_file")"

# print filtered out list
grep --invert-match --file=<(echo "$excludes") <<< "$commits"
